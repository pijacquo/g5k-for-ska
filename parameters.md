# Optimisation with parameters

Here is the list of the importants parameters of DDFacet and killMS, with their meaning, and their impact on performance.

## DDFacet

### Parallelisation parameters

* --Parallel-NCPU=N : Number of CPU to use in parallel mode. 0 : use all, 1 : sequential. (default value : 0)
* --Parallel-Affinity=P : enable hyperthreading : 1 enable, 2 disable. (default value : 1) **Maybe 1 does not enable HyperThreading on all type of CPU** 
> Parallel-Affinity defines the step used while selecting cores. If the step is 1, the selected cores will be 0,1,2,3,4, etc. Selecting cores with this step includes a physical core, and its virtual core (and by doing so, activating hyperthreading). Using a step of 2 will only select physical cores : 0,2,4,6 etc. In this case hyperthreading will not be enabled.
> I noticed that --Parallel-Affinity=2 decreases perfomances. You should use the default value .
* --Parallel-MainProcessAffinity : ? (default : 0) 
* --GAClean-NCPU=N : Number of CPU to use in parallel mode. **In parallel mode during SSD deconvolution, DDF.py can get blocked (maybe a deadlock ?). Setting it to 1 resolve *sometime* the problem.**

If you're digging into DDFacet's source code, there is a README for AsynchProcessPool on DDFacet's repo : [APP readme](https://github.com/cyriltasse/DDFacet/blob/master/DDFacet/Other/README-APP.md). There are plenty of informations useful about how processes are handled. Beware, some parts of the program (DeconvSSD for example) use directly python Multiprocessing library.

### Cache parameters

Most of cache parameters are enabled by default, reducing computation time. So I chose to put in this list only cache parameters which are not activated by default.

* --Cache-HMP=0|1 : Cache HMP basis function. May be faster to recompute. (default value : 0)

### Parameters affecting performance ( & quality )

* --Deconv-PeakFactor=X : set minor cycle-stopping threshold to X\*{peak residual at start of a major cycle}. (default value : 0.15)
* --Deconv-CycleFactor=X used to set a minor cycle stopping threshold based on PSF sidelobe level. Use 0 to disable. 2.5 is a "reasonable" value, but may lead to very superficial data treatement.
* --Deconv-RMSFactor=X: set minor cycle stopping threshold for residual. (X\*residual at start). (default : 0.0)
* --Deconv-MaxMajorIter=N : number of major cycles (default : 20)
* --Deconv-MaxMajorIter=N : number of minor cycles iterations (default : 20000)
* --Image-Npix=N : image size (default 5000)
* -- CF-wmax=METERS : maximum w coordinates. Visibilities with larger w will not be gridded.
* --Facets-NFacets=N: number of facets to use. (default : 3)
* --Freq-NDegridBand=N : numbr of image bands for degridding. 0 means degrid each channel (default 0)
* --Freq-NBand=N : number of image band for gridding. (default : 1)
* --Beam-NBand=N : numbr of channels over wich same beam value is used. (0 by default, means every channel)
* --Data-Sort=0|1 : if true, data will be resorted inernally. Speeds up processing. (default : false)
### Other parameters

These are mask parameters. They must be specified to use Masks generated with MakeMask.

* --Mask-External=FILENAME : use a clean mask ( at .fits format ). (default value : None)
* --Mask-Auto=MASK-AUTO : do automatic masking (default : false)
* --Mask-SigTh=Threshold : set threshold for automatic masking (default : 10)
<!-- TODO: what is this parameter ?
Mask-FluxImageType=MASK-FLUXIMAGETYPE : if Auto enabled, does the cut of sigTh  either on restored image or ModelConv (default : ModelConv) ?? 
-->

* --DDESolutions-DDSols=SOLUTION-NAME : name of the solutions computed by killMS. **Use this if you want to generate a Direction Dependant Image.** (default : None !)

### DDF.py command

DFF.py should always be used with at least these parameters :
* --Ouput-Name
* --Cache-Dir
* --Data-MS
* --Data-ColName (DATA)
* --Data-Sort
* --Deconv-Mode
* --Cache-HMP (if using --Deconv-Mode=HMP)
* --Output-Also
* --Parallel-NCPU

If using `--Deconv-Mode=SSD`, you should also use `--GAClean-NCPU=1`. This algorithm is buggy, and while using CPU the algorithm can get stuck in a deconvolution phase. Although this parameter does not improve perfomance, it can permit the algorithm to finish... But it does not seems to work every time. Instead of using the SSD Deconv mode, you should rather use the one by default : HMP.

Here is a DDF.py call with basics options :
```
DDF.py --Output-Name=/tmp/dest/image_DI --Cache-Dir=/tmp/cache --Data-MS mslist.txt --Data-ColName DATA --Data-Sort=1 --Cache-HMP=1 --Output-Also onNeds
```

For now, I haven't found how to generate a picture human-friendly.

## MakeMask

* --RestoredIm=RESTOREDIMAGE : name of the restored image to mask.
* --OutName=OUTNAME : name of the generated file. if not specified, add `.mask.fits` to RESTOREDIMAGE.

```
MakeMask.py --RestoredIm=image_DI.residual01
```
This command will create a masked image called image_DI.residual01.mask.fits.

## MakeModel

There is not much to say about MakeModel. Clustering sky model with it is really quick, and there is not many parameters. Don't forget to disable plot with `--DoPlot=0`. If you did not, MakeModel will try to display a plot on screen with matplotlib, wich can be quite a problem on a node without graphical interface...

* --DoPlot=0|1 : plot curve at the end of clustering. Crash if you have no GUI installed ! (default value : 1)
* --MaskName=MASK_NAME : name of the mask to use to mas the DicoModel.
* --NCluster=N : number of direction to cluster the sky.


#### Clustering the sky
```
MakeModel.py --BaseImageName image_DI --DoPlot=0 --NCluster=10
```

## killMS

### Cache parameters

* --DDFCacheDir=path : If you specified a different cache directory than you current working directory you should specify it here so that kMS will be able to use DDF's cache.

### Parameters affecting performance

* --NCPU : Number of cores to use. **default value : 1 no parallelism !**
* --NThread=N : Number of thread to use. (default : 1) **Seems to have no effects on performance and processes !**

### Other parameters

* --NodesFile : name of the npy.ClusterCat.npy file generated during the clustering step. 
### kMS.py command 

kMS should always be used with these parameters :

* --MSName
* --BaseImageName
* --DDFCacheDir
* --InCol
* --OutSolsName
* --NCPU
* --NodesFile (if you clustered the sky before)

Use `--BaseImageName` to indicate the name of the files generated by DDF.py, they are used by kMS.py.

There is also a parameter `--OutCol`, used for naming the column of the data generated by kMS in the dataset. By default it is set to `CORRECTED_DATA`. If you absolutely want to change it, do not set it to `DATA`, because it will overwrite the original data used for benchmarks !

`--OutSolsName` sets a name for the solution computed by kMS. They are used by DDF.py during the last step, and there is no default values for it, so its setting is mandatory !

```
kMS.py --MSName=mslist.txt --BaseImageName=image_DI --DDFCacheDir=/tmp/cache --InCol DATA --OutSolsName kafca_sols --NodesFile image_DI.npy.ClusterCat.npy
```

## Command usage examples

### Data reduction

This is inspired from the example that can be read on [killMS README](https://github.com/cyriltasse/killMS). I just changed a few parameter, to match with our environment and make it finish earlier :


#### making an "image" Direction Independant 

```
DDF.py --Output-Name=/tmp/dest/image_DI --Cache-Dir=/tmp/cache --Data-MS mslist.txt --Data-ColName DATA --Deconv-MaxMajorIter=3 --Deconv-CycleFactor=2.5 --Data-Sort=1 --Cache-HMP=1 --Output-Also onNeds --Mask-Auto=1 --Mask-SigTh=5.00
```
This command use the `--Cache-HMP=1` option, beacause HMP algorithm is the value by default for `--Deconv-Mode`.

#### Clustering sky
```
MakeModel.py --BaseImageName image_DI --NCluster 10 --DoPlot=0 
```

#### Calibrate Measurement Set (MS)

We solve for Jones Matrix, supposing the sky is "known".

```
kMS.py --MSName=mslist.txt --BaseImageName=image_DI --DDFCacheDir=/tmp/cache --InCol DATA --OutSolsName kafca_sols --NodesFile image_DI.npy.ClusterCat.npy

```

#### Making an "image" Direction Dependant :

Use the Jones Matrix computed with KMS to refine our sky data. 
```
DDF.py --Output-Name=image_DD --Data-MS mslist.txt --Data-ColName CORRECTED_DATA --DDESolutions-DDSols kafca_sols --Output-Also onNeds --Data-Sort 1 --Cache-Dir=/tmp/cache --Cache-Reset 0 --Predict-InitDicoModel image_DI.DicoModel
```

You must use the parameter `--DDESolutions-DDSols` so that DDF can use the solutions computed by kMS. Of course, it need to be the same name as the one used for the parameter `--OutSolsName` of kMS (look above to find it). 
