#!/bin/bash

#init.sh is a script that copy files required to run
#the DDFacet + killMS pipeline on a node.
#It is used by the deploy.py python script, but you can
#also use it yourself, if you just want to test something
#on one node.

cmdFile=
msFile=
#launchFile=
if [ $# -lt 3 ] && [ "$2" != "-d" ]
then
	echo -e "Error : parameter(s) missing !"
	echo -e "Command syntax : ./init.sh node cmdFile mslist.txt ..."
	echo -e "\t\t OR"
	echo -e "\t\t./init.sh -d testFolder ..."
	echo -e "Where testFolder is the path to a folder containing commands, mslist.txt files"
	echo -e "see https://gitlab.inria.fr/pijacquo/g5k-for-ska for more information."
else
	node="$1"
	if [ "$2" = "-d" ]
	then
		#$2 is the folder where are located files to rsync.
		shift
		testFolder="$2"

		cd "$testFolder"
		echo "Rsync $testFolder files."
		cmdFile="commands"
		msFile="mslist.txt"
		#launchFile="../g5k-for-ska/tools/launch.sh"
	else
		cmdFile="$2"
		msFile="$3"
		#launchFile="$4"	
	fi	

	echo "$cmdFile -> root@$node:commands"
	rsync "$cmdFile" root@$node:commands

	echo "$msFile -> root@$node:mslist.txt"
	rsync "$msFile" root@$node:mslist.txt

	#echo "$launchFile -> root@$node:launch.sh"
	#rsync "$launchFile" root@$node:launch.sh

	while [ -n "$5" ]
	do
		filename=${5##*/}
		echo "$5 -> root@$node:$filename"
		rsync "$5" root@$node:$filename
		shift
	done

fi
