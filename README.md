# g5k for SKA
This repo contains tools and information for using the LOFAR pipeline (killMS and DDFacet) on Grid5000.

Commands, librairies and programs installed, and scripts are working with :

* [DDFacet](https://github.com/saopicc/DDFacet) v0.4.1.0
* killMS v2.7.0.0

Because killMS is still in development, some things may have changed since my internship. Stay informed of the situation of killMS by looking at the repo and the issues on github.

# Repository organisation

**benchmarks** contains calc files with measures taken from programs execution.

**docs** contains text files : reports, analysis, documentation...

**tools** contains useful scripts for simplify your life.

**modified_code** contains files where I modified code for testing, getting values... The structure of directories inside is the same as DDFacet's and killMS architecture, so that a python file is at the same place in both reposotories, relatively to DDFacet (and killMS) folder.
> Example : In saopicc's DDFacet repo, there is a file DDFacet/DDFacet/Array/shared_dict.py. Is is located in this repo at /modified_code/DDFacet/DDFacet/Array/shared_dict.py

[imglofar.md](https://gitlab.inria.fr/pijacquo/g5k-for-ska/blob/master/imglofar.md) contains information about setting up your environment to make running the pipeline.

[parameters.md](https://gitlab.inria.fr/pijacquo/g5k-for-ska/blob/master/parameters.md) contains information about how to execution DDFacet and killMS : commands, useful parameters etc.
