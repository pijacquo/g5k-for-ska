#!/bin/bash

user=
site_front=
cacheDir=/tmp/cache
destDir=/tmp/dest
scriptDir=`pwd`
msFile=mslist.txt
accessFile=NFS
timeFile=$scriptDir/timelist.csv
cmdFile=$scriptDir/commands

#columns declarations for csv time file.
columns="operation;real-time;time (s);user-time;system-time;op time;inputs;outputs;min pagefaults;maj pagefault;swaps;"

#command used to store execution time into timeFile.
measure_time="/usr/bin/time --format %E;;%U;%S;;%I;%O;%R;%M;%W;"

while [ -n "$1" ]
do
	case "$1" in

		"-c")
			shift
			cacheDir="$1"
			;;

		"-d")
			shift
			destDir="$1"
			;;

		"-f")
			shift
			columns="operation;$1"
			measure_time="/usr/bin/time --format $1"
			;;

		"-m")
			shift
			msFile="$1"
			;;

		"-t")
			shift
			timeFile="$1"
			;;

		"-u")
			shift
			user="$1"
			;;

		"-s")
			shift
			site_front="$1"
			;;

		"--local")
			accessFile="local"
			;;

		*)
			cmdFile="$1"
			;;
	esac
	shift
done

# Add timeFile to command at the last time in case it has
# been changed by user.
measure_time="$measure_time -a -o $timeFile"

mkdir $cacheDir $destDir

if [ $accessFile = "local" ]
then
	dataPath=/tmp/data
	mkdir $dataPath
	touch tmp_msfile.txt
	while read ms_path
	do
		ms_name=`echo "$ms_path" | cut -f6 -d/`
		rsync -ravhPu $ms_path $dataPath/
		echo "$dataPath/$ms_name" >> tmp_msfile.txt
		echo "$ms_name : $ms_path -> $dataPath/$ms_name"
	done <<< "$(cat $msFile)"
	cp tmp_msfile.txt $destDir/$msFile
	rm tmp_msfile.txt	
else
	cp $msFile $destDir/$msFile
fi

touch $timeFile
echo "$columns" > $timeFile
source /root/venv/bin/activate

cd $destDir

while read line
do
	description=`echo "$line" | cut -f1 -d:`
	cmd=`echo "$line" | cut -f2 -d:`
	echo -n "$description;" >> $timeFile
	#echo "executing $cmd"
       	$measure_time $cmd	
done <<< "$(cat "$cmdFile")"

if [ -n "$user" ] && [ -n "$site_front" ]
then
	rsync $timeFile $user@$site_front:timelist/$timeFile
fi
