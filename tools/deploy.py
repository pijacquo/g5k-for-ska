#!/usr/bin/env python3
"""
This python script reserve one or several node, and
does all the necessary to launch the pipeline remotly.

command syntax : 
    python deploy.py <path to command file> <path to msfile>
"""

from execo import *
from execo_g5k import *
import itertools
import sys

def init_pipeline(node, folder):
    """
        run init script for copying files onto nodes.
        commands and msfile must be in the folder given in
        parameter.
        (commands file, launch.sh, msfile...)
    """
    cmd = "./init.sh " + node
    cmd = cmd + " -d " + folder
    #TODO: get username with env variable.

    init_process = Process(cmd, shell=True)
    init_process.start()
    init_process.wait()
    
def launch_pipeline(deployed):
    """
        run launch.sh, executing pipeline and measuring
        time execution
    """
    connec_param = execo.config.default_connection_params
    connec_param['user'] = "root"
    pipeline_process = Remote("./launch.sh", 
                                deployed,
                                connection_params=connec_param)
    pipeline_process.start()
    pipeline_process.wait()

if __name__ == "__main__":
    args = sys.argv[1:]
    sql_prop = None
    if len(args) >= 1:
        print("Will oarsub for a cluster :", args[0])
        sql_prop = "cluster='{}'".format(args[0])
    
    [(jobid, site)] = oarsub([
        (OarSubmission( sql_properties=sql_prop, 
                        walltime=7200, 
                        job_type="deploy"), 
                    "lille")
        ])
    
    print("Oarsub finished.\nJob", jobid, "on", site)
    
    if jobid:
        try:
            nodes = []
            wait_oar_job_start(jobid, site)
            nodes = get_oar_job_nodes(jobid, site)
            node_name = (get_host_attributes(nodes[0]))['uid']
            print("Deploying imglofar on", node_name, "...")
            deployment = Deployment(hosts=nodes, 
                            env_file="/home/pjacquot/images/imglofar/imglofar.env", 
                            user="pjacquot")
            deploy(deployment)
            print("Deployment successful !")
            print("Log on with : ssh root@", node_name, "!")
        except:
            print("Error during deployment. Aborting operation.")
            oardel([(jobid, site)])
            print("Job ", jobid, " deleted.")

